## Modified commands
alias s="screen"
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias l='ls -CF'
alias ls='ls -p --color=auto'
alias grep='grep --color=auto'
function cdl { cd $1; ls;}
#alias ssh="~/.scripts/ssh/ssh-ident"

# Enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Utility commands
alias mpc="ncmpcpp"
alias emc="em -nw"
alias mu4e="em -c -n --eval '(mu4e)'"
alias apagar="sudo shutdown -h 14:20"
alias acortar='curl -s "http://is.gd/create.php?format=simple&url=`xsel -po`" | xsel -pi'
alias bajar="plowdown"
alias subir="plowup"
alias search-word="find . -type f -print0 | xargs -0 grep -l $1"
alias jpg-optimized="find . -name *.jpg -exec jpegoptim --max=80 -t '$i' {} \;"
alias prime-enable="export DRI_PRIME=1"
alias prime-disable="export DRI_PRIME=0"
alias webcam="webcam_mods bg-blur"

# Custom ~/.scripts commands
alias pass="$HOME/.scripts/pass-utils/passp"
alias pomf="$HOME/.scripts/pomf.py"
alias pomf-w="$HOME/.scripts/scrotpomf.sh"
alias capas2png="$HOME/.scripts/Inkscape/layers2pngs.py"
alias tvgnu="$HOME/.scripts/TVenGNU.sh"
alias hastebin="$HOME/.scripts/haste.sh"
alias haste="HASTE_SERVER=http://vte.distopico.info haste"
alias davpush="$HOME/.scripts/davpush.pl"

# Virtualenv
vePath="$HOME/.virtualenv/bin"
alias lstream="${vePath}/livestreamer-curses"

# Alias directories
alias home="cd $HOME/"
alias musica="cd $HOME/Music"
alias descargas="cd $HOME/Downloads"
alias escritorio="cd $HOME/Desktop"
alias publico="cd $HOME/Public"
alias videos="cd $HOME/Videos"
alias imagenes="cd $HOME/Pictures"

# Update Grub2 Parabola
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

# Pkg manager
alias repo="sudo add-apt-repository $1"
alias build-dep="sudo apt-get build-dep $1"
alias update-full="sudo aptitude full-upgrade -y && sudo aptitude clean"
alias search-app="sudo aptitude search"
alias dist-upgrade="sudo apt-get dist-upgrade"

# Guix alias
alias guix-reconfigure="sudo guix system reconfigure /etc/config.scm; sudo update-boot"

# Fix some paths until I migrate to guix/nix shells workflow
_LIBRARY_PATH=/lib:/run/current-system/profile/lib:$HOME/.guix-profile/lib:/gnu/store/gmv6n5vy5qcsn71pkapg2hnknyn1p7g3-gcc-12.3.0-lib/lib:$LIBRARY_PATH
_LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$_LIBRARY_PATH
alias node="LD_LIBRARY_PATH=$_LD_LIBRARY_PATH node $@"
alias npx="LD_LIBRARY_PATH=$_LD_LIBRARY_PATH npx $@"
alias npm="LD_LIBRARY_PATH=$_LD_LIBRARY_PATH npm $@"
alias pnpm="LD_LIBRARY_PATH=$_LD_LIBRARY_PATH pnpm $@"

# Fix xmonad --recompile that doesn't found their onw libs
GHC_VERSION=9.2.5
XMONAD_VERSION=0.17.1
_GHC_PACKAGE_PATH=$GHC_PACKAGE_PATH:/run/current-system/profile/lib/ghc-$GHC_VERSION/xmonad-$XMONAD_VERSION.conf.d:/run/current-system/profile/lib/ghc-$GHC_VERSION/package.conf.d:/run/current-system/profile/lib/ghc-$GHC_VERSION/ghc-xmonad-contrib-$XMONAD_VERSION.conf.d
# alias xmonad="GHC_PACKAGE_PATH=$_GHC_PACKAGE_PATH LD_LIBRARY_PATH=$_LD_LIBRARY_PATH xmonad $@"
# alias xmobar="LD_LIBRARY_PATH=$_LD_LIBRARY_PATH xmobar $@"
# alias ghcup="LD_LIBRARY_PATH=$_LD_LIBRARY_PATH ghcup $@"
# alias ghc="LD_LIBRARY_PATH=$_LD_LIBRARY_PATH ghc $@"
# alias ghc-pkg="LD_LIBRARY_PATH=$_LD_LIBRARY_PATH ghc-pkg $@"
# alias cabal="LD_LIBRARY_PATH=$_LD_LIBRARY_PATH cabal $@"
# alias haskell-language-server-wrapper="LD_LIBRARY_PATH=$_LD_LIBRARY_PATH haskell-language-server-wrapper $@"
